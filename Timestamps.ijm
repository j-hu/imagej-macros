/*	Jennifer Hu
 *	21 December 2018
 *	Setup:
 *	- Specify channel numbers.
 *	- Organize folders and images in dir as follows:
 *			dir
 *			 - raw (czi images to process)
 *			 - ortho (pngs of ortho views)
 *			 - czi (processed czi images transferred from raw)
 *			 - measurements (results file)
 *			 - failed (images that I can't find the cells in)
 *	Usage:
 *	- Select dir.
 *  
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// interval (minutes) between timepoints & initial time
interval = 20;
start = 0;

// prompt info from user
dir_source = getDirectory("Choose directory to timestamp:");
dir = File.getParent(dir_source);
dir_destination = dir+"Timestamped"+File.separator;

// iterate over files in dir_source
list = getFileList(dir_source);
k = list.length;
for (i=0; i<k; i++) {
	// open image and go to center slice
	run("Bio-Formats Windowless Importer", "open=["+dir_source+filename+"]");
	// remove ".czi" for image name
	name = File.nameWithoutExtension;
	// get number of channels and z-slices
	Stack.getDimensions(w, h, channels, slices, frames);
	
	// creates timestamp in upper left
	run("Label...", "format=00:00 starting=0 interval="+interval+" x=5 y=20 font=18 text=[] range=1-"+frames);

	// save as tif stack? can't do czi...
}